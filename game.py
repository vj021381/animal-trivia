from .get_input import get_valid_input
from game_v1.animal_data.animals import *
from game_v1.animal_data.quiz_questions import *
from game_v1.ascii_art.art import *
from game_v1.ascii_art.mammals import *
from game_v1.animal_data.animal_discriptions import animal_descriptions
import time
import os

def start_game():
    """ This function is responsible for the game logic """ 
    animal_type = get_valid_input('>>> ', animal_types)
    print(f'\nYou have selected {animal_type.title()}\n')
    display_animal_description(animal_type=animal_type)
    display_ascii_art_and_animal_disription(animal_type)
    set_quiz_questions = f'quiz_questions_{animal_type}'
    quiz_questions = globals()[set_quiz_questions]
    print('\n')
    initiate_quiz(quiz_questions)

    play_more_prompt = (
        "Would like to try out the trivia for an other animal-type?\n"
        """Pick an animal type for which you'd like to know more about or just enter "quit" to exit the game\n\n"""
        "Mammals - Birds - Reptiles - Fish - Insects - Amphibians\n"
    )
    print_text(play_more_prompt, delay=1, line_by_line=True)
    start_game()

def display_animal_description(animal_type=False, animal_name=False):
    if animal_name:
        animal = animal_name
        description = animal_descriptions.get(animal)
    elif animal_type:
        animal = animal_type
        description = animal_types_discription.get(animal)

    print_text(description, delay=0.25)
    time.sleep(1.5)

def initiate_quiz(quiz_questions):
    score = 0
    for qa in quiz_questions:
        print(f"\t\t\t{qa['question']}")
        for i, option in enumerate(qa["options"]):
            print(f"{i+1}. {option}")
        user_answer = get_valid_input("\nEnter your answer: ", qa["options"])
        if user_answer == qa["answer"].strip().lower():
            print("\nCorrect!\n")
            score += 1
        else:
            time.sleep(1)
            print(
                "\nIncorrect :(\n"
                f"The correct answer was {qa['answer']}\n"
            )
    print(
        "Thanks for playing!\n"
        f'You got {score} out of {len(quiz_questions)} questions correct!'
    )

def print_text(dialog, delay=1, line_by_line=False):
    if line_by_line:
        for line in dialog.splitlines():
            print(line, flush=True)
            time.sleep(delay)
    else:
        words = dialog.split()
        count = 0
        for word in words:
            count += 1
            print(word, end=' ', flush=True)
            if count % 15 == 0:
                print()
            time.sleep(delay)

def display_ascii_art_and_animal_disription(animal_type):
    animals = {
        'mammals': ascii_mammals,
        'birds': Eagle,
        'reptiles': Crocodile,
        'fish': Shark,
        'insects': Butterfly,
        'amphibians': Frog
    }
    try:
        if animal_type == 'mammals':
            for animal in animals[animal_type]:
                animal_name = check_for_animal_name(animal)
                print(animal)
                time.sleep(2)
                display_animal_description(animal_name=animal_name)
                time.sleep(2)
        else:
            print(animals[animal_type])
    except KeyError:
        return

def check_for_animal_name(string):
    for animal in animals_used:
        if animal in string:
            return animal

