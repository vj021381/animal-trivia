import sys

def get_valid_input(prompt: str, valid_choices: list):
    """
    Get input from user and validates it against a list of valid choices.

    :param prompt: The prompt message to display to the user.
    :param valid_choices: A list of valid choices.
    :return: The user's input, if it is valid.
    """
    valid_choices = [x.lower() for x in valid_choices]

    while True:
        try:
            user_input = input(prompt).lower().strip()
            if not user_input:
                print("You didn't enter anything.")
                continue
            if user_input == 'quit':
                print("\nThanks for playing Animal Trivia!")
                sys.exit()
            if user_input.lower() not in valid_choices:
                print(f"Invalid input. Please enter one of the following: {', '.join(valid_choices)}")
                continue
            return user_input
        except ValueError:
            print("You must enter a valid string.")
