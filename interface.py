from .get_input import get_valid_input
from .game import start_game
import sys

COMMANDS = ["play", "help", "quit"]
DIFFICULTY_LEVELS = ["easy", "medium", "hard"]

def interface():
    print(
        "\nWelcome to Animail Trivia.\n"
        "Enter 'play' to start the game, 'help' for instructions or 'quit' at any point in the game to exit.\n"
    )
    prompt = get_valid_input('>>> ', COMMANDS)
    if prompt == "play":
        print("\nSelect an animal type: Mammals, birds, reptiles, fish, insects, amphibians\n")
        start_game()
    elif prompt == "help":
        help_menu()
    elif prompt == "quit":
        sys.exit()

def help_menu():
    message = ("\nWelcome to Animal Trivia!\n\n"
               "In this game, you will be presented with multiple-choice questions about animals.\n"
               "You have to select the correct answer among the options provided.\n"
               "You will get 1 point for each correct answer and there is no penalty for incorrect answers.\n"
               "You can quit the game at any time by typing 'quit' at the prompt.\n\n"
               "Good luck and have fun!\n")
    print(message)
    prompt = get_valid_input("Enter 'play' to start the game or 'quit' to exit: ", ["play","quit"])
    if prompt == "play":
        print("\nSelect an animal type: Mammals, birds, reptiles, fish, insects, amphibians\n")
        start_game()
    elif prompt == "quit":
        sys.exit()
