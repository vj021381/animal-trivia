animal_types = ["mammals", "birds", "reptiles", "fish", "insects", "amphibians"]

animals_used = ["Elephant", "Lion", "Giraffe", "Hippopotamus", "Gorilla", "Eagle", "Parrot", "Penguin", "Ostrich", "Swan","Crocodile", "Turtle", "Lizard", "Python", "Iguana","Salmon", "Tuna", "Goldfish", "Shark", "Clownfish","Butterfly", "Bee", "Ant", "Dragonfly", "Ladybug","Frog", "Salamander", "Caecilian", "Newt", "Toad"]

animal_types_discription = {
    "mammals":
    """
Mammals are a diverse group of warm-blooded, vertebrate animals that have fur or hair and feed their young with milk from mammary glands. Some interesting facts about mammals include:
They are found on every continent and in every type of environment, from the Arctic to the Antarctic and from deserts to rainforests.
They are the only group of animals with the ability to fly (bats) and the only group that can live both on land and in water (whales, dolphins, and porpoises).
They have highly developed brains and are capable of advanced behaviors such as tool use, problem-solving, and complex communication.""",
    "birds":
    """
Birds are a diverse group of warm-blooded, feathered, egg-laying vertebrates that have the ability to fly. Here are some interesting facts about birds:
Birds are found on every continent and in every type of environment, from the Arctic to the Antarctic and from deserts to rainforests.
They have lightweight skeletons and powerful muscles that allow them to fly. Some birds, such as penguins, have adapted to life on land and can no longer fly, but retain the characteristics of birds.
Some birds, such as the ostrich and emu, are flightless and can run fast.""",
    "reptiles":
    """
Reptiles are a diverse group of cold-blooded, vertebrate animals that are characterized by their scaly skin. Here are some interesting facts about reptiles:
Reptiles are found on every continent except Antarctica, and they live in a variety of environments, including deserts, wetlands, and rainforests.
They have scaly skin that protects them from the elements and helps to retain moisture.
They are cold-blooded, which means that their body temperature is regulated by the environment around them.
They lay eggs, which are often leathery or hard-shelled to protect them from drying out or being crushed.
Some reptiles, such as lizards and snakes, can regrow lost tails or limbs.""",
    "fish": 
    """
Fish are found in every ocean and in many freshwater habitats, from small ponds to large rivers and lakes.
They are cold-blooded, which means that their body temperature is regulated by the environment around them.
Fish have gills, which are specialized organs that extract oxygen from the water so that they can breathe.
They have fins and scales that help them to swim and to protect their bodies.
Fish come in a wide variety of shapes, sizes, and colors, from the tiny neon tetra to the massive whale shark.""",
    "insects":
    """
Insects are a diverse group of arthropods that are characterized by their three-part body (head, thorax, and abdomen), three pairs of legs, and two pairs of wings. Here are some interesting facts about insects:
Insects are found in almost every habitat on earth, from deserts to rainforests, and from the polar regions to the tropics.
They are the most diverse group of animals, with over a million described species and many more still to be discovered.
Insects have a unique body structure that includes a exoskeleton, which provides support and protection for their bodies.""",
    "amphibians":
    """
Amphibians are a diverse group of cold-blooded, vertebrate animals that are characterized by their ability to live both on land and in water. Here are some interesting facts about amphibians:
Amphibians are found on every continent except Antarctica, and they live in a variety of environments, including wetlands, forests, and deserts.
They are cold-blooded, which means that their body temperature is regulated by the environment around them.
Amphibians undergo metamorphosis, which is a process of developmental change that includes a change from a aquatic larval stage to a terrestrial adult stage.
They have moist, permeable skin that allows them to breathe through their skin as well as through their lungs.""" 
}

