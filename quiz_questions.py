quiz_questions_mammals = [
    {'question': 'What is the largest land animal?', 'options': ['Elephant', 'Lion', 'Giraffe', 'Hippopotamus', 'Gorilla'], 'answer': 'Elephant'},
    {'question': 'Which mammal is known for its distinctive mane?', 'options': ['Elephant', 'Lion', 'Giraffe', 'Hippopotamus', 'Gorilla'], 'answer': 'Lion'},
    {'question': 'What mammal has a long neck and legs?', 'options': ['Elephant', 'Lion', 'Giraffe', 'Hippopotamus', 'Gorilla'], 'answer': 'Giraffe'},
    {'question': 'What mammal is known for its large size and semi-aquatic lifestyle?', 'options': ['Elephant', 'Lion', 'Giraffe', 'Hippopotamus', 'Gorilla'], 'answer': 'Hippopotamus'},
    {'question': 'What mammal is known for its intelligence and social behavior?', 'options': ['Elephant', 'Lion', 'Giraffe', 'Hippopotamus', 'Gorilla'], 'answer': 'Gorilla'},
    
]

quiz_questions_birds = [
    {'question': 'What bird is known for its keen eyesight and hunting abilities?', 'options': ['Eagle', 'Parrot', 'Penguin', 'Ostrich', 'Swan'], 'answer': 'Eagle'},
    {'question': 'What bird is known for its ability to mimic human speech?', 'options': ['Eagle', 'Parrot', 'Penguin', 'Ostrich', 'Swan'], 'answer': 'Parrot'},
    {'question': 'What bird is known for its flightless nature and distinctive black-and-white plumage?', 'options': ['Eagle', 'Parrot', 'Penguin', 'Ostrich', 'Swan'], 'answer': 'Penguin'},
    {'question': 'What bird is known for its large size and fast running speed?', 'options': ['Eagle', 'Parrot', 'Penguin', 'Ostrich', 'Swan'], 'answer': 'Ostrich'},
    {'question': 'What bird is known for its graceful swimming and elegant appearance?', 'options': ['Eagle', 'Parrot', 'Penguin', 'Ostrich', 'Swan'], 'answer': 'Swan'},

]

quiz_questions_reptiles = [
    {'question': 'What reptile is known for its large size and aggressive nature?', 'options': ['Crocodile', 'Turtle', 'Lizard', 'Python', 'Iguana'], 'answer': 'Crocodile'},
    {'question': 'What reptile is known for its hard shell and slow-moving nature?', 'options': ['Crocodile', 'Turtle', 'Lizard', 'Python', 'Iguana'], 'answer': 'Turtle'},
    {'question': 'What reptile is known for its ability to change color?', 'options': ['Crocodile', 'Turtle', 'Lizard', 'Python', 'Iguana'], 'answer': 'Lizard'},
    {'question': 'What reptile is known for its large size and constricting abilities?', 'options': ['Crocodile', 'Turtle', 'Lizard', 'Python', 'Iguana'], 'answer': 'Python'},
    {'question': 'What reptile is known for its green color and tree-dwelling nature?', 'options': ['Crocodile', 'Turtle', 'Lizard', 'Python', 'Iguana'], 'answer': 'Iguana'},

]

quiz_questions_fish = [
    {'question': 'What fish is known for its pink color and freshwater habitat?', 'options': ['Salmon', 'Tuna', 'Goldfish', 'Shark', 'Clownfish'], 'answer': 'Salmon'},
    {'question': 'What fish is known for its high-quality meat and ocean habitat?', 'options': ['Salmon', 'Tuna', 'Goldfish', 'Shark', 'Clownfish'], 'answer': 'Tuna'},
    {'question': 'What fish is known for its small size and bright orange color?', 'options': ['Salmon', 'Tuna', 'Goldfish', 'Shark', 'Clownfish'], 'answer': 'Goldfish'},
    {'question': 'What fish is known for its large size and predatory nature?', 'options': ['Salmon', 'Tuna', 'Goldfish', 'Shark', 'Clownfish'], 'answer': 'Shark'},
    {'question': 'What fish is known for its vibrant colors and association with coral reefs?', 'options': ['Salmon', 'Tuna', 'Goldfish', 'Shark', 'Clownfish'], 'answer': 'Clownfish'},
    
]

quiz_questions_insects = [
    {'question': 'What insect is known for its colorful wings and aerial acrobatics?', 'options': ['Butterfly', 'Bee', 'Ant', 'Dragonfly', 'Ladybug'], 'answer': 'Butterfly'},
    {'question': 'What insect is known for its role in pollination and honey production?', 'options': ['Butterfly', 'Bee', 'Ant', 'Dragonfly', 'Ladybug'], 'answer': 'Bee'},
    {'question': 'What insect is known for its highly organized social colonies?', 'options': ['Butterfly', 'Bee','Ant', 'Dragonfly', 'Ladybug'], 'answer': 'Ant'},
    {'question': 'What insect is known for its long, slender body and aerial agility?', 'options': ['Butterfly', 'Bee', 'Ant', 'Dragonfly', 'Ladybug'], 'answer': 'Dragonfly'},
    {'question': 'What insect is known for its round shape and red-with-black-spots appearance?', 'options': ['Butterfly', 'Bee', 'Ant', 'Dragonfly', 'Ladybug'], 'answer': 'Ladybug'},

]

quiz_questions_amphibians = [
    {'question': 'What amphibian is known for its green color and ability to jump long distances?', 'options': ['Frog', 'Salamander', 'Caecilian', 'Newt', 'Toad'], 'answer': 'Frog'},
    {'question': 'What amphibian is known for its smooth, moist skin and ability to regenerate lost limbs?', 'options': ['Frog', 'Salamander', 'Caecilian', 'Newt', 'Toad'], 'answer': 'Salamander'},
    {'question': 'What amphibian is known for its burrowing lifestyle and lack of visible limbs?', 'options': ['Frog', 'Salamander', 'Caecilian', 'Newt', 'Toad'], 'answer': 'Caecilian'},
    {'question': 'What amphibian is known for its smooth skin and aquatic-to-terrestrial lifestyle?', 'options': ['Frog', 'Salamander', 'Caecilian', 'Newt', 'Toad'], 'answer': 'Newt'},
    {'question': 'What amphibian is known for its dry, bumpy skin and terrestrial habitat?', 'options': ['Frog', 'Salamander', 'Caecilian', 'Newt', 'Toad'], 'answer': 'Toad'}

]
